import React from 'react';
import Home from './pages/home';
import Auth from './pages/auth';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
} from "react-router-dom";
import './styles/App.scss';
import Button from "./components/Button";

function App() {
  return (
      <Router>
        <div className="App">
            <div className="container">
                <header>
                    <div>Приложение-задачник</div>
                    <Link to="/auth">Вход</Link>
                </header>
                <Switch>
                    <Route path="/auth">
                        <Auth/>
                    </Route>
                    <Route path="/">
                        <Home />
                    </Route>
                </Switch>
            </div>
        </div>
      </Router>
  );
}

export default App;
