import React, {useEffect, useState} from "react";
import Button from "../../components/Button";
import "./PopupAddTask.scss"

const PopupAddNewTask  = ({close}) => {
    const [ userName, setUserName ] = useState('');
    const [ email, setEmail ] = useState('');
    const [ taskText, setTaskText ] = useState('');

    const [ error, setError ] = useState('');

    useEffect(() => {

    }, []);

    const AddTask = () => {
        if (!userName) {
            setError('Незаполнено имя пользователя')
        }
        if (!email) {
            setError('Незаполнен email')
        }
        if (!taskText) {
            setError('Незаполнен текст задачи')
        }

    };
    return (
       <div className="PopUpWrapper">
           <div className="PopupAddNewTask">
               <div className="PopupAddNewTask__title">
                   <span>Новая задача</span>
                   <div className="PopupAddNewTask__close" onClick={close}>x</div>
               </div>
               <div className="PopupAddNewTask__body">
                   {error && <div className="PopupAddNewTask__errors">{error}</div>}
                   <input type="text" placeholder="Имя пользователя" onChange={e => setUserName(e.target.value)}/>
                   <input type="text" placeholder="E-mail" onChange={e => setEmail(e.target.value)}/>
                   <textarea placeholder="Текст задачи" onChange={e => setTaskText(e.target.value)}/>
                   <Button onClick={AddTask} title="Добавить задачу"/>
               </div>
            </div>
       </div>
    )
};

export default PopupAddNewTask;