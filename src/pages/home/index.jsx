import React, { useState } from "react";
import Button from "../../components/Button";
import PopupAddTask from "../../components/PopupAddTask"
import "./home.scss";



const Home  = () => {
    const [showPopUpAddTask, setShowPopUpAddTask] = useState(false);
    return (
        <>
            <div className="home">
                <div className="task-list">
                    <div className="task-list__title">
                        <span>Список задач</span>
                        <Button title="Добавить задачу" onClick={() => setShowPopUpAddTask(true)}/>
                    </div>
                    <div className="sorting">
                        <div className="sorting__list">
                            <div className="sorting__list__item">Имя пользователя</div>
                            <div className="sorting__list__item">Email</div>
                            <div className="sorting__list__item">Статус</div>
                        </div>
                    </div>
                    <div className="task-list__list">

                    </div>
                </div>
            </div>
            { showPopUpAddTask && <PopupAddTask close={() => setShowPopUpAddTask(false)} /> }
        </>
    )
};

export default Home;